package utils

import "math/rand"

func GenerateIntBetween(min int, max int) int {
	return rand.Intn(max-min) + min
}
