package utils

import (
	"math/rand"
	"time"
)

var (
	charset    = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"
	charsetLen = len(charset)
)

func GenerateRandomString(size int) string {
	seededRand := rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, size)

	for i := range b {
		b[i] = charset[seededRand.Intn(charsetLen)]
	}

	return string(b)
}
