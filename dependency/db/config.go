package db

import (
	"github.com/Netflix/go-env"
)

type Config struct {
	URI  string `env:"DB_URI"`
	Name string `env:"DB_NAME"`
}

func NewConfig() (*Config, error) {
	var cfg Config

	_, err := env.UnmarshalFromEnviron(&cfg)

	if err != nil {
		return nil, err
	}

	return &cfg, err
}
