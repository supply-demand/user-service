package db

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
)

var (
	WCMajority     = writeconcern.New(writeconcern.WMajority())
	RCSnapshot     = readconcern.Snapshot()
	writeExExample = mongo.WriteException{}
)

type TxManager struct {
	client *mongo.Client
}

func NewTxManager(client *mongo.Client) *TxManager {
	return &TxManager{
		client: client,
	}
}

func (tx *TxManager) Strong(
	ctx context.Context,
	cb func(sessionCtx mongo.SessionContext) error,
) error {
	return tx.Wrap(ctx, cb, WCMajority, RCSnapshot)
}

func (tx *TxManager) IsErrorNeedRetry(err error) bool {
	writeExc, ok := err.(mongo.WriteException)
	if !ok {
		return false
	}

	for _, label := range writeExc.Labels {
		if label == "TransientTransactionError" {
			return true
		}
	}

	return false
}

func (tx *TxManager) IsCommitNeedRetry(err error) bool {
	writeExc, ok := err.(mongo.WriteException)
	if !ok {
		return false
	}

	for _, label := range writeExc.Labels {
		if label == "UnknownTransactionCommitResult" {
			return true
		}
	}

	return false
}

func (tx *TxManager) Wrap(
	ctx context.Context,
	cb func(sessionCtx mongo.SessionContext) error,
	wc *writeconcern.WriteConcern,
	rc *readconcern.ReadConcern,
) error {
	txnOpts := options.Transaction().SetWriteConcern(wc).SetReadConcern(rc)

	session, err := tx.client.StartSession()
	if err != nil {
		return err
	}

	defer session.EndSession(ctx)

	err = mongo.WithSession(ctx, session, func(sessionCtx mongo.SessionContext) error {
		if err := session.StartTransaction(txnOpts); err != nil {
			return err
		}

		if err := cb(sessionCtx); err != nil {
			return err
		}

		if err = session.CommitTransaction(sessionCtx); err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		if errAbort := session.AbortTransaction(ctx); errAbort != nil {
			return errAbort
		}

		return err
	}

	return nil
}
