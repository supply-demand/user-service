package db

import (
	"context"
	"errors"
	"sync"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func NewClient(config *Config) (*mongo.Client, error) {
	return mongo.NewClient(options.Client().ApplyURI(config.URI))
}

func ProvideDatabaseFromClient(client *mongo.Client, config *Config) *mongo.Database {
	return client.Database(config.Name)
}

type ConnectionManager struct {
	client      *mongo.Client
	config      *Config
	db          *mongo.Database
	isConnected bool
	mutex       sync.Mutex
}

func NewConnectionManager(config *Config, client *mongo.Client) (*ConnectionManager, error) {
	conn := &ConnectionManager{
		config: config,
		client: client,
	}

	return conn, nil
}

func (c *ConnectionManager) Connect(ctx context.Context) error {
	if c.isConnected {
		return errors.New("attempt to connect twice")
	}

	c.mutex.Lock()
	defer c.mutex.Unlock()

	if err := c.client.Connect(ctx); err != nil {
		return err
	}

	c.isConnected = true

	return nil
}

func (c *ConnectionManager) Disconnect(ctx context.Context) error {
	if !c.isConnected {
		return errors.New("cannot close not opened connection")
	}

	c.mutex.Lock()
	defer c.mutex.Unlock()

	if err := c.client.Disconnect(ctx); err != nil {
		return err
	}

	c.isConnected = false

	return nil
}

func (c *ConnectionManager) Client() *mongo.Client {
	if !c.isConnected {
		panic("database is not connected")
	}

	return c.client
}

// Db returns database instance. Do not call in constructor
func (c *ConnectionManager) Db() *mongo.Database {
	if !c.isConnected {
		panic("database is not connected")
	}

	if c.db == nil {
		c.db = c.client.Database(c.config.Name)
	}

	return c.db
}
