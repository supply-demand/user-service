package db

import (
	"context"

	"go.uber.org/fx"
)

func RegisterModule(lc fx.Lifecycle, manager *ConnectionManager) {
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return manager.Connect(ctx)
		},
		OnStop: func(ctx context.Context) error {
			return manager.Disconnect(ctx)
		},
	})
}

var Module = fx.Options(
	fx.Provide(
		NewConfig,
		NewConnectionManager,
		NewTxManager,
		NewClient,
		ProvideDatabaseFromClient,
	),
	fx.Invoke(RegisterModule),
)
