package db

import "gitlab.com/supply-demand/auth/shared/utils"

type ETagPair struct {
	Old string
	New string
}

func NewETagPair(from string) ETagPair {
	return ETagPair{
		Old: from,
		New: GenerateETag(),
	}
}

func GenerateETag() string {
	return utils.GenerateRandomString(10)
}
