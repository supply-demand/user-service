package dependency

import (
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go/config"
)

func NewTracer() (opentracing.Tracer, error) {
	cfg, err := config.FromEnv()
	if err != nil {
		return nil, err
	}

	tracer, closer, err := cfg.NewTracer()
	if err != nil {
		return nil, err
	}
	defer closer.Close()

	opentracing.SetGlobalTracer(tracer)

	return tracer, nil
}
