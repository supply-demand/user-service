package user

import (
	"gitlab.com/supply-demand/auth/domain/user/actions"
	"go.uber.org/fx"
)

var Module = fx.Provide(actions.NewUserService)
