package actions

import (
	"context"

	"gitlab.com/supply-demand/auth/dependency/db"
)

type ChangePassword struct {
	ID           string
	ETag         string
	PasswordHash string
}

type ChangePasswordResult struct {
	ETag string
}

func (s *Service) ChangePassword(ctx context.Context, dto *ChangePassword) (*ChangePasswordResult, error) {
	etagPair := db.NewETagPair(dto.ETag)

	err := s.userRepository.UpdatePass(ctx, etagPair, dto.ID, dto.PasswordHash)
	if err != nil {
		return nil, err
	}

	return &ChangePasswordResult{
		ETag: etagPair.New,
	}, nil
}
