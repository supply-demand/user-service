package actions

import (
	"context"

	"gitlab.com/supply-demand/auth/dependency/db"

	userStorage "gitlab.com/supply-demand/auth/storage/user"
)

type ChangeStatus struct {
	ID     string
	ETag   string
	Status userStorage.Status
}

type ChangeStatusResult struct {
	ETag string
}

func (s *Service) ChangeStatus(ctx context.Context, dto *ChangeStatus) (*ChangeStatusResult, error) {
	etagPair := db.NewETagPair(dto.ETag)

	err := s.userRepository.UpdateStatus(ctx, etagPair, dto.ID, dto.Status)
	if err != nil {
		return nil, err
	}

	return &ChangeStatusResult{
		ETag: etagPair.New,
	}, nil
}
