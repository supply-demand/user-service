package actions

import (
	"context"

	userStorage "gitlab.com/supply-demand/auth/storage/user"
)

type FindByEmailAndPassword struct {
	Email        string
	PasswordHash string
}

func (s *Service) FindByEmailAndPassword(ctx context.Context, dto *FindByEmailAndPassword) (*userStorage.User, error) {
	user, err := s.userRepository.FindByEmail(ctx, dto.Email)
	if err != nil {
		return nil, err
	}

	if user.PasswordHash != dto.PasswordHash {
		return nil, userStorage.ErrUserNotFound
	}

	return user, nil
}
