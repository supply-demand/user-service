package actions

import (
	"context"

	"gitlab.com/supply-demand/auth/dependency/db"
)

type RemoveUserByID struct {
	ID   string
	ETag string
}

type RemoveUserByIDResult struct {
	ETag string
}

func (s *Service) Remove(ctx context.Context, dto *RemoveUserByID) (*RemoveUserByIDResult, error) {
	etag := db.NewETagPair(dto.ETag)

	if err := s.userRepository.Remove(ctx, etag, dto.ID); err != nil {
		return nil, err
	}

	return &RemoveUserByIDResult{
		ETag: etag.New,
	}, nil
}
