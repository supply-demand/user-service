package actions

import (
	"gitlab.com/supply-demand/auth/dependency/db"
	"gitlab.com/supply-demand/auth/shared/utils"

	userStorage "gitlab.com/supply-demand/auth/storage/user"
)

type Service struct {
	userRepository *userStorage.Repository
	txManager      *db.TxManager
}

type FindByID struct {
	ID string
}

func NewUserService(userRepository *userStorage.Repository, txManager *db.TxManager) *Service {
	return &Service{
		userRepository: userRepository,
		txManager:      txManager,
	}
}

func (s *Service) generateETag() string {
	return utils.GenerateRandomString(10)
}
