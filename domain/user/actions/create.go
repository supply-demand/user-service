package actions

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"

	userStorage "gitlab.com/supply-demand/auth/storage/user"
)

type CreateUser struct {
	Name         string
	Email        string
	Phone        string
	PasswordHash string
}

type CreateUserResult struct {
	ID   string
	ETag string
}

// Create TODO: add user-id to duplicate error
func (s *Service) Create(ctx context.Context, dto *CreateUser) (*userStorage.User, error) {
	user := &userStorage.User{
		ID:           primitive.NewObjectID(),
		Email:        dto.Email,
		Name:         dto.Name,
		PasswordHash: dto.PasswordHash,
		ETag:         s.generateETag(),
		Status:       userStorage.StatusNew,
		CreatedAt:    time.Now(),
	}

	if err := s.userRepository.Insert(ctx, user); err != nil {
		return nil, err
	}

	return user, nil
}
