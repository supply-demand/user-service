package actions

import (
	"context"

	userStorage "gitlab.com/supply-demand/auth/storage/user"
)

func (s *Service) FindByID(ctx context.Context, id string) (*userStorage.User, error) {
	return s.userRepository.FindByID(ctx, id)
}
