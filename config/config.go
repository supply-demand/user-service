package config

import "github.com/Netflix/go-env"

type EnvType string

const (
	EnvProd  EnvType = "prod"
	EnvStage EnvType = "stage"
	EnvDev   EnvType = "dev"
	EnvLocal EnvType = "local"
)

type Config struct {
	Env EnvType `env:"ENV,default=prod"`
}

func NewConfig() (*Config, error) {
	var cfg Config

	_, err := env.UnmarshalFromEnviron(&cfg)

	if err != nil {
		return nil, err
	}

	return &cfg, err
}
