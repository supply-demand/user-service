ARG PKG_REGISTRY
#ARG PKG_REGISTRY_PATH
ARG PKG_REGISTRY_LOGIN
ARG PKG_REGISTRY_PASS

FROM golang:1.16.6-alpine as scratch
#ENV LANG=C.UTF-8
#RUN apk update
#RUN apk --no-cache --update add git gcc g++
RUN go env -w GO111MODULE=on
#RUN go env -w GOPRIVATE=$PKG_REGISTRY/$PKG_REGISTRY_PATH
RUN go env -w GOPRIVATE=$PKG_REGISTRY
RUN echo "machine $PKG_REGISTRY login $PKG_REGISTRY_LOGIN password $PKG_REGISTRY_PASS" > $HOME/.netrc

FROM scratch as dev

FROM scratch AS build
WORKDIR /project
COPY . .
ENV ENV=prod
RUN GOOS=linux GARCH=amd64 CGO_ENABLED=0 go build -o user-service .
RUN chmod +x user-service

FROM scratch as prod
COPY --from=build /project/user-service .
RUN ln -s /project/user-service /usr/bin/user-service
CMD ["user-service"]
