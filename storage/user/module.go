package user

import (
	"context"

	"go.uber.org/fx"
)

func RegisterModule(lc fx.Lifecycle, repo *Repository) {
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return repo.PrepareCollection(ctx)
		},
		OnStop: func(ctx context.Context) error {
			return nil
		},
	})
}

var Module = fx.Options(
	fx.Provide(
		NewUserRepository,
	),
	fx.Invoke(
		RegisterModule,
	),
)
