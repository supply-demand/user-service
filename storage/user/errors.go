package user

import "errors"

var ErrUserNotFound = errors.New("user not found")

var ErrUserConflict = errors.New("user conflict")
