package user

import (
	"context"

	"gitlab.com/supply-demand/auth/dependency/db"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// TODO: throw particular error in case of ETag mismatch

type Repository struct {
	database *mongo.Database
}

func NewUserRepository(database *mongo.Database) *Repository {
	return &Repository{
		database: database,
	}
}

func (r *Repository) FindByEmail(ctx context.Context, email string) (*User, error) {
	res := r.collection().FindOne(ctx, bson.M{"email": email})
	if err := res.Err(); err != nil {
		return nil, r.mapDocumentError(err)
	}

	var user User
	if err := res.Decode(&user); err != nil {
		return nil, err
	}

	return &user, nil
}

func (r *Repository) FindByID(
	ctx context.Context,
	id string,
) (*User, error) {
	res := r.collection().FindOne(ctx, bson.M{
		"id": id,
	})
	if err := res.Err(); err != nil {
		return nil, r.mapDocumentError(err)
	}

	var user User
	if err := res.Decode(&user); err != nil {
		return nil, r.mapDocumentError(err)
	}

	return &user, nil
}

func (r *Repository) Insert(ctx context.Context, payload *User) error {
	_, err := r.collection().InsertOne(ctx, payload)

	return r.mapDocumentError(err)
}

func (r *Repository) UpdateStatus(ctx context.Context, etag db.ETagPair, id string, status Status) error {
	res, err := r.collection().UpdateOne(ctx, bson.M{
		"id":   id,
		"etag": etag.Old,
	}, bson.M{
		"status": status,
		"etag":   etag.New,
	})
	if err != nil {
		return err
	}

	if res.ModifiedCount == 0 {
		return ErrUserNotFound
	}

	return nil
}

func (r *Repository) UpdatePass(ctx context.Context, etag db.ETagPair, id string, password string) error {
	res, err := r.collection().UpdateOne(ctx, bson.M{
		"id":   id,
		"etag": etag.Old,
	}, bson.M{
		"password": password,
		"etag":     etag.New,
	})
	if err != nil {
		return err
	}

	if res.ModifiedCount == 0 {
		return ErrUserNotFound
	}

	return nil
}

func (r *Repository) Remove(ctx context.Context, etag db.ETagPair, id string) error {
	res, err := r.collection().UpdateOne(ctx, bson.M{
		"id":   id,
		"etag": etag.Old,
	}, bson.M{
		"status": StatusRemoved,
		"etag":   etag.New,
	})
	if err != nil {
		return err
	}

	if res.ModifiedCount > 0 {
		return ErrUserNotFound
	}

	return nil
}

func (r *Repository) PrepareCollection(ctx context.Context) error {
	yes := true

	_, err := r.collection().Indexes().CreateOne(ctx, mongo.IndexModel{
		Keys: bson.M{
			"email": 1,
		},
		Options: &options.IndexOptions{
			Unique: &yes,
		},
	})
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) mapDocumentError(err error) error {
	if err == nil {
		return nil
	}

	if mongo.IsDuplicateKeyError(err) {
		return ErrUserConflict
	} else if err == mongo.ErrNoDocuments {
		return ErrUserNotFound
	}

	return err
}

func (r *Repository) collection() *mongo.Collection {
	return r.database.Collection("users")
}
