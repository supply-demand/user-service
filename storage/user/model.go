package user

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Status int

var (
	StatusNew       Status = 1
	StatusConfirmed Status = 2
	StatusRemoved   Status = 3
)

type User struct {
	ID           primitive.ObjectID `bson:"_id"`
	Name         string             `bson:"name"`
	Email        string             `bson:"email"`
	Phone        string             `bson:"phone"`
	Status       Status             `bson:"status"`
	ETag         string             `bson:"etag"`
	PasswordHash string             `bson:"password_hash"`
	CreatedAt    time.Time          `bson:"created_at"`
}
