package prometheus

import (
	"context"

	"go.uber.org/fx"
)

func registerModule(lc fx.Lifecycle, manager *ServerManager) {
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return manager.Start(ctx)
		},
		OnStop: func(ctx context.Context) error {
			return manager.Stop(ctx)
		},
	})
}

var Module = fx.Options(
	fx.Provide(
		NewConfig,
		NewServer,
		NewServerManager,
	),
	fx.Invoke(
		registerModule,
	),
)
