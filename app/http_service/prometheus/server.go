package prometheus

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func NewServer(cfg *Config) *http.Server {
	metricsHandler := promhttp.Handler()

	return &http.Server{
		Addr: fmt.Sprintf(":%d", cfg.Port),
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "/metrics" {
				metricsHandler.ServeHTTP(w, r)
			}

			w.WriteHeader(http.StatusNotImplemented)
		}),
	}
}

type ServerManager struct {
	server *http.Server
	errCh  chan error
	logger *zap.Logger
}

func NewServerManager(server *http.Server, errCh chan error, logger *zap.Logger) *ServerManager {
	return &ServerManager{
		server: server,
		errCh:  errCh,
		logger: logger,
	}
}

func (m *ServerManager) Start(ctx context.Context) error {
	go func() {
		if err := m.server.ListenAndServe(); err != nil {
			m.errCh <- err
		}
	}()

	select {
	case <-time.After(100 * time.Millisecond):
		break
	case err := <-m.errCh:
		return err
	}

	m.logger.Info(fmt.Sprintf("Prometheus server on address %s is started", m.server.Addr))

	return nil
}

func (m *ServerManager) Stop(ctx context.Context) error {
	return m.server.Shutdown(ctx)
}
