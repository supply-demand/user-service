package prometheus

import "github.com/Netflix/go-env"

type Config struct {
	Port int `env:"PROMETHEUS_PORT,default=80"`
}

func NewConfig() (*Config, error) {
	var cfg Config

	_, err := env.UnmarshalFromEnviron(&cfg)

	if err != nil {
		return nil, err
	}

	return &cfg, err
}
