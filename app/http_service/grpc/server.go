package grpc

import (
	"github.com/opentracing/opentracing-go"
	"gitlab.com/supply-demand/auth/app/http_service/grpc/handlers"
	"gitlab.com/supply-demand/proto_gen/go/user"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_opentracing "github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
)

func NewGrpcServer(
	userHandler *handlers.UserHandler,
	logger *zap.Logger,
	tracer opentracing.Tracer,
) (*grpc.Server, error) {
	s := grpc.NewServer(
		grpc.ChainUnaryInterceptor(
			grpc_zap.UnaryServerInterceptor(logger),
			grpc_prometheus.UnaryServerInterceptor,
			grpc_opentracing.UnaryServerInterceptor(grpc_opentracing.WithTracer(tracer)),
			grpc_recovery.UnaryServerInterceptor(),
		),
	)

	reflection.Register(s)
	user.RegisterUserServiceServer(s, userHandler)

	return s, nil
}
