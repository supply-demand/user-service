package grpc

import (
	"fmt"
	"go.uber.org/zap"
	"net"
	"time"

	"google.golang.org/grpc"
)

type ServerManager struct {
	cfg        *Config
	grpcServer *grpc.Server
	logger     *zap.Logger
	errChan    chan error
}

func NewServerManager(grpcServer *grpc.Server, cfg *Config, errChan chan error, logger *zap.Logger) *ServerManager {
	return &ServerManager{
		cfg:        cfg,
		errChan:    errChan,
		logger:     logger,
		grpcServer: grpcServer,
	}
}

func (m *ServerManager) Start() error {
	addr := fmt.Sprintf("%s:%d", m.cfg.GrpcServerAddr, m.cfg.GrpcServerPort)
	socket, err := net.Listen("tcp", addr)

	if err != nil {
		return err
	}

	m.logger.Info(fmt.Sprintf("TCP socket on address %s is opened", addr))

	go func() {
		if err := m.grpcServer.Serve(socket); err != nil {
			m.errChan <- err
		}
	}()

	select {
	case <-time.After(100 * time.Millisecond):
		break
	case err := <-m.errChan:
		return err
	}

	return nil
}

func (m *ServerManager) Stop() error {
	m.grpcServer.GracefulStop()

	return nil
}
