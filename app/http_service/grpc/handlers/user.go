package handlers

import (
	"context"
	"errors"
	"fmt"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"

	userDomain "gitlab.com/supply-demand/auth/domain/user/actions"
	userStorage "gitlab.com/supply-demand/auth/storage/user"
	userProto "gitlab.com/supply-demand/proto_gen/go/user"
	spb "google.golang.org/genproto/googleapis/rpc/status"
)

type UserHandler struct {
	userProto.UnimplementedUserServiceServer
	userService *userDomain.Service
}

func NewUserHandler(userService *userDomain.Service) *UserHandler {
	return &UserHandler{
		userService: userService,
	}
}

func (h *UserHandler) Create(
	ctx context.Context,
	args *userProto.CreateArgs,
) (*userProto.CreateResult, error) {
	user, err := h.userService.Create(ctx, &userDomain.CreateUser{
		Email:        args.GetEmail(),
		PasswordHash: args.GetPasswordHash(),
		Name:         args.GetName(),
	})
	if err != nil {
		return nil, h.mapError(err)
	}

	return &userProto.CreateResult{
		UserID: user.ID.String(),
		Etag:   user.ETag,
	}, nil
}

func (h *UserHandler) Remove(
	ctx context.Context,
	args *userProto.RemoveArgs,
) (*userProto.RemoveResult, error) {
	user, err := h.userService.Remove(ctx, &userDomain.RemoveUserByID{
		ID:   args.GetId(),
		ETag: args.GetEtag(),
	})
	if err != nil {
		return nil, h.mapError(err)
	}

	return &userProto.RemoveResult{
		Etag: user.ETag,
	}, nil
}

func (h *UserHandler) ChangeStatus(
	ctx context.Context,
	args *userProto.ChangeStatusArgs,
) (*userProto.ChangeStatusResult, error) {
	statusStorage, err := h.mapStatusFromProto(args.GetStatus())
	if err != nil {
		return nil, err
	}

	user, err := h.userService.ChangeStatus(ctx, &userDomain.ChangeStatus{
		ID:     args.GetId(),
		ETag:   args.GetEtag(),
		Status: statusStorage,
	})
	if err != nil {
		return nil, h.mapError(err)
	}

	return &userProto.ChangeStatusResult{
		Etag: user.ETag,
	}, nil
}

func (h *UserHandler) ChangePass(
	ctx context.Context,
	args *userProto.ChangePassArgs,
) (*userProto.ChangePassResult, error) {
	user, err := h.userService.ChangePassword(ctx, &userDomain.ChangePassword{
		ID:           args.GetId(),
		ETag:         args.GetEtag(),
		PasswordHash: args.GetPass(),
	})
	if err != nil {
		return nil, h.mapError(err)
	}

	return &userProto.ChangePassResult{
		Etag: user.ETag,
	}, nil
}

func (h *UserHandler) FindByID(
	ctx context.Context,
	args *userProto.FindByIDArgs,
) (*userProto.FindByIDResult, error) {
	user, err := h.userService.FindByID(ctx, args.GetId())
	if err != nil {
		return nil, h.mapError(err)
	}

	userPb, err := h.mapUser(user)
	if err != nil {
		return nil, err
	}

	return &userProto.FindByIDResult{
		User: userPb,
	}, nil
}

func (h *UserHandler) FindByEmailAndPassword(
	ctx context.Context,
	args *userProto.FindByEmailAndPasswordArgs,
) (*userProto.FindByEmailAndPasswordResult, error) {
	user, err := h.userService.FindByEmailAndPassword(ctx, &userDomain.FindByEmailAndPassword{
		Email:        args.GetEmail(),
		PasswordHash: args.GetPasswordHash(),
	})
	if err != nil {
		return nil, h.mapError(err)
	}

	userPb, err := h.mapUser(user)
	if err != nil {
		return nil, err
	}

	return &userProto.FindByEmailAndPasswordResult{
		User: userPb,
	}, nil
}

func (h *UserHandler) mapUser(u *userStorage.User) (*userProto.User, error) {
	userStatus, err := h.mapStatusFromLocal(u.Status)
	if err != nil {
		return nil, err
	}

	return &userProto.User{
		Email:     u.Email,
		Name:      u.Name,
		Status:    userStatus,
		Etag:      u.ETag,
		CreatedAt: timestamppb.New(u.CreatedAt),
	}, nil
}

func (h *UserHandler) mapError(err error) error {
	if err == userStorage.ErrUserNotFound {
		return status.ErrorProto(&spb.Status{
			Code:    int32(codes.AlreadyExists),
			Message: "Conflict",
		})
	}

	return err
}

func (h *UserHandler) mapStatusFromLocal(status userStorage.Status) (userProto.Status, error) {
	if status == userStorage.StatusNew {
		return userProto.Status_New, nil
	} else if status == userStorage.StatusConfirmed {
		return userProto.Status_Confirmed, nil
	} else if status == userStorage.StatusRemoved {
		return userProto.Status_Removed, nil
	}

	return 0, errors.New(fmt.Sprintf("Could not map status %d", status))
}

func (h *UserHandler) mapStatusFromProto(status userProto.Status) (userStorage.Status, error) {
	if status == userProto.Status_New {
		return userStorage.StatusNew, nil
	} else if status == userProto.Status_Confirmed {
		return userStorage.StatusConfirmed, nil
	} else if status == userProto.Status_Removed {
		return userStorage.StatusRemoved, nil
	}

	return 0, errors.New(fmt.Sprintf("Could not map status %d", status))
}
