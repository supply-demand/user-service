package grpc

import (
	"github.com/Netflix/go-env"
)

type Config struct {
	GrpcServerPort int    `env:"GRPC_SERVER_PORT,default=8010"`
	GrpcServerAddr string `env:"GRPC_SERVER_ADDR,default=127.0.0.1"`
}

func NewConfig() (*Config, error) {
	cfg := Config{}

	_, err := env.UnmarshalFromEnviron(&cfg)

	if err != nil {
		return nil, err
	}

	return &cfg, err
}
