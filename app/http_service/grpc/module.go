package grpc

import (
	"context"

	"gitlab.com/supply-demand/auth/app/http_service/grpc/handlers"
	"go.uber.org/fx"
)

func Run(lc fx.Lifecycle, manager *ServerManager) {
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return manager.Start()
		},
		OnStop: func(ctx context.Context) error {
			return manager.Stop()
		},
	})
}

var Module = fx.Options(
	handlers.Module,
	fx.Provide(
		NewConfig,
		NewGrpcServer,
		NewServerManager,
	),
	fx.Invoke(
		Run,
	),
)
