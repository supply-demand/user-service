package http_service

import (
	"go.uber.org/zap"
)

type Runner struct {
	errCh  chan error
	stopCh chan bool
	logger *zap.Logger
}

func NewRunner(errCh chan error, logger *zap.Logger) *Runner {
	return &Runner{
		errCh:  errCh,
		stopCh: make(chan bool),
		logger: logger,
	}
}

func (r *Runner) Start() error {
	go func() {
		for {
			select {
			case err := <-r.errCh:
				r.logger.With(zap.Error(err)).Info("Error propagated to the top")
			case <-r.stopCh:
				break
			}
		}
	}()

	return nil
}

func (r *Runner) Stop() error {
	return nil
}
