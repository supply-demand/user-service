package http_service

import (
	"context"

	"gitlab.com/supply-demand/auth/app/http_service/grpc"
	"gitlab.com/supply-demand/auth/app/http_service/prometheus"
	"gitlab.com/supply-demand/auth/config"
	"gitlab.com/supply-demand/auth/dependency"
	"gitlab.com/supply-demand/auth/dependency/db"
	"go.uber.org/fx"
	"go.uber.org/fx/fxevent"
	"go.uber.org/zap"

	userDomain "gitlab.com/supply-demand/auth/domain/user"
	userStorage "gitlab.com/supply-demand/auth/storage/user"
)

func registerModule(lc fx.Lifecycle, runner *Runner) {
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return runner.Start()
		},
		OnStop: func(ctx context.Context) error {
			return runner.Stop()
		},
	})
}

func Run() error {
	appConfig, err := config.NewConfig()
	if err != nil {
		return err
	}

	logger, err := zap.NewProduction()
	if err != nil {
		return err
	}
	defer logger.Sync()

	var errChan chan error

	opts := []fx.Option{
		fx.Supply(appConfig),
		fx.Supply(logger),
		fx.Supply(errChan),
		db.Module,
		grpc.Module,
		prometheus.Module,
		fx.Provide(
			dependency.NewTracer,
			NewRunner,
		),
		userStorage.Module,
		userDomain.Module,
		fx.Invoke(
			registerModule,
		),
		fx.WithLogger(func(logger *zap.Logger) fxevent.Logger {
			return &fxevent.ZapLogger{Logger: logger}
		}),
	}

	app := fx.New(opts...)

	app.Run()

	return app.Err()
}
